package xyz.stepsecret.ioio;

import ioio.lib.api.DigitalOutput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends IOIOActivity {

    private ToggleButton toggleButton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toggleButton1 = (ToggleButton) findViewById(R.id.toggleButton1);



    }

    class Looper extends BaseIOIOLooper {

        private DigitalOutput digital_led0;



        @Override
        protected void setup() throws ConnectionLostException, InterruptedException {

            digital_led0 = ioio_.openDigitalOutput(1,true);



            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "IOIO Connect", Toast.LENGTH_SHORT).show();
                }
            });



        }

        @Override
        public void loop() throws ConnectionLostException {

            digital_led0.write(!toggleButton1.isChecked());



            try  {
                Thread.sleep(100);
            }catch(InterruptedException e){

            }

        }
    }

    @Override
    protected IOIOLooper createIOIOLooper() {
        return new Looper();
    }
}
